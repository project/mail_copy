<?php

/**
 * @file
 * Routines for the Blind Carbon Copy module.
 * Based on no more maintained bcc module.
 * Special thanks to its original maintainers.
 */

define('MAIL_COPY_EXCLUDE_ID', 0);
define('MAIL_COPY_INCLUDE_ID', 1);

/**
 * Implements hook_menu().
 */
function mail_copy_menu() {
  $items = array();
  $items['admin/config/system/mail-copy'] = array(
    'title' => 'Mail Copy',
    'description' => 'Administer automated mail copy configuration.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mail_copy_admin_settings'),
    'access arguments' => array('administer mail_copy'),
    'file' => 'mail_copy.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function mail_copy_permission(){
  return array(
    'administer mail_copy' => array(
      'title' => t('Administer Mail Copy'),
      'description' => t('Change Mail Copy settings and e-mail copy addresses.'),
    ),
  );
}

/**
 * Implements hook_mail_alter().
 *
 * If MAIL_COPY is enabled, inject the configured email address (or the site
 * email) into a Mail_copy header in the email that is about to be sent.
 *
 * @see drupal_mail()
 */
function mail_copy_mail_alter(&$message) {
  if (variable_get('mail_copy_enabled', 0)) {
    // Check whether MAIL_COPY is enabled for this mail ID.
    if (_mail_copy_id_match($message['id'])) {
      if (empty($message['headers']['cc'])) {
        $message['headers']['cc'] = variable_get('mail_copy_mail_cc_addresses', variable_get('site_mail', ''));
      }
      else {
        $message['headers']['cc'] = $message['headers']['cc'] . ',' . variable_get('mail_copy_mail_cc_addresses', '');
      }

      if (empty($message['headers']['bcc'])) {
        $message['headers']['bcc'] = variable_get('mail_copy_mail_bcc_addresses', variable_get('site_mail', ''));
      }
      else {
        $message['headers']['bcc'] = $message['headers']['bcc'] . ',' . variable_get('mail_copy_mail_bcc_addresses', variable_get('site_mail', ''));
      }
      _mail_copy_debug('E-mail copy was sent to the following recipients: CC: @cc, BCC: @bcc', array('@cc' => $message['headers']['cc'], '@bcc' => $message['headers']['bcc']));
    } else {
      _mail_copy_debug('E-mail copy was NOT sent due to configured settings.');
    }
  }
}

/**
 * Check if the current mail ID should have a MAIL_COPY header injected.
 *
 * @param $mail_id
 *   An id to identify the e-mail sent.
 *
 * @return bool
 *   TRUE or FALSE.
 */
function _mail_copy_id_match($mail_id) {
  $list = trim(variable_get('mail_copy_mail_ids', ''));
  $include_listed = variable_get('mail_copy_mail_visibility', MAIL_COPY_EXCLUDE_ID);
  if (!empty($list)) {
    $id_matches = drupal_match_path($mail_id, $list);
    if($id_matches){
      _mail_copy_debug('Outgoing mail DOES match mail copy mail_id list: "@mail_id", Mode: @mode', array('@mail_id' => $mail_id, '@mode' => ($include_listed ? 'Include mode' : 'Exclude mode')));
    } else {
      _mail_copy_debug('Outgoing mail does NOT match mail copy mail_id list: "@mail_id", Mode: @mode', array('@mail_id' => $mail_id, '@mode' => ($include_listed ? 'Include mode' : 'Exclude mode')));
    }
  } else {
    _mail_copy_debug('Mail IDs excludes / includes list is empty. Mail ID to compare: "@mail_id", Mode: @mode', array('@mail_id' => $mail_id, '@mode' => ($include_listed ? 'Include mode' : 'Exclude mode')));
  }

  $is_match = $id_matches xor empty($include_listed);
  return $is_match;
}

function _mail_copy_debug($message, array $variables = array()){
  $debug = variable_get('mail_copy_debug', FALSE);
  if($debug){
    watchdog('mail_copy', $message, $variables, WATCHDOG_DEBUG);
  }
}
