<?php

/**
 * @file
 * Settings for for the mail_copy module.
 */

/**
 * Return the settings form.
 *
 * Enable MAIL_COPY and specify an email address that should be injected.
 */
function mail_copy_admin_settings() {
  $form['mail_copy_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Mail Copy'),
    '#description' => t('Choose whether you want Drupal to send a mail copy for outgoing emails.'),
    '#default_value' => variable_get('mail_copy_enabled', 0),
  );

  $form['mail_copy_mail_cc_addresses'] = array(
    '#type' => 'textfield',
    '#title' => t('CC e-mail addresses'),
    '#description' => t('Enter the e-mail addresses that should receive visible (CC) copies of all site e-mails. Separate multiple addresses by comma (,) without spaces.'),
    '#default_value' => variable_get('mail_copy_mail_cc_addresses', ''),
    '#states' => array(
      'visible' => array(
        ":input[name='mail_copy_enabled']" => array('checked' => TRUE))),
  );

  $form['mail_copy_mail_bcc_addresses'] = array(
    '#type' => 'textfield',
    '#title' => t('BCC e-mail addresses'),
    '#description' => t('Enter the e-mail addresses that should receive hidden (BCC) copies of all site e-mails. Separate multiple addresses by comma (,) without spaces.'),
    '#default_value' => variable_get('mail_copy_mail_bcc_addresses', variable_get('site_mail', '')),
    '#states' => array(
      'visible' => array(
        ":input[name='mail_copy_enabled']" => array('checked' => TRUE))),
  );

  $form['mail_copy_mail_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Include / exclude specific e-mail IDs'),
    '#description' => t('Select how you would like to handle exclusions / inclusions of specific Drupal mail IDs. Select ´All mail IDs except those listed´ and leave the IDs textarea blank to receive copies of all e-mails.'),
    '#options' => array(
      MAIL_COPY_EXCLUDE_ID => t('All mail IDs except those listed'),
      MAIL_COPY_INCLUDE_ID => t('Only the listed mail IDs'),
    ),
    '#default_value' => variable_get('mail_copy_mail_visibility', MAIL_COPY_EXCLUDE_ID),
    '#states' => array(
      'visible' => array(
        ":input[name='mail_copy_enabled']" => array('checked' => TRUE))),
  );

  $form['mail_copy_mail_ids'] = array(
    '#type' => 'textarea',
    '#title' => t('E-mail IDs to exclude / include'),
    '#description' => t("Specify system mails by using their IDs. Enter one ID per line. The '*' character is a wildcard.<br />Example IDs are %user_register_no_approval_required for the account creation notification email and %user_password_reset for the password reset email. Mail IDs pattern is MODULENAME_MAILKEY.", array('%user_register_no_approval_required' => 'user_register_no_approval_required', '%user_password_reset' => 'user_password_reset')),
    '#default_value' => variable_get('mail_copy_mail_ids', ''),
    '#states' => array(
      'visible' => array(
        ":input[name='mail_copy_enabled']" => array('checked' => TRUE))),
  );

  $form['mail_copy_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable mail copy debug') . ' ' . t('(experimental)'),
    '#description' => t('Log mail copy comparison to watchdog for debugging. You may also use this functionality to determine specific e-mail IDs and disable this setting in production.'),
    '#default_value' => variable_get('mail_copy_debug', 0),
    '#states' => array(
      'visible' => array(
        ":input[name='mail_copy_enabled']" => array('checked' => TRUE))),
  );

  return system_settings_form($form);
}

/**
 * Validate handler for the settings form.
 */
function mail_copy_admin_settings_validate(&$form, $form_state) {
  if ($form_state['values']['mail_copy_enabled']) {
    // CC addresses:
    _mail_copy_validate_email_addresses('mail_copy_mail_cc_addresses', $form_state['values']['mail_copy_mail_cc_addresses']);
    // BCC addresses:
    _mail_copy_validate_email_addresses('mail_copy_mail_bcc_addresses', $form_state['values']['mail_copy_mail_bcc_addresses']);
  }
  // Tidy up the list of IDs.
  $form_state['values']['mail_copy_mail_ids'] = trim($form_state['values']['mail_copy_mail_ids']);
}

/**
 * Splits the given email addresses by comma and returns them by array.
 *
 * @param $email_addresses string
 *   The email addresses string.
 *
 * @return array
 */
function _mail_copy_split_email_addresses($email_addresses) {
  $email_addresses_array = explode(',', trim($email_addresses));
  $email_addresses_array = array_map('trim', $email_addresses_array);
  return $email_addresses_array;
}

/**
 * Helper function to validate the given email addresses string.
 *
 * @param $field_name String
 *   The name of the field (form_set_error) which the email address belongs to.
 * @param string $email_addresses
 *   A list of email addresses separated by comma.
 */
function _mail_copy_validate_email_addresses($field_name, $email_addresses = '') {
  if (!empty($email_addresses)) {
    $email_addresses_array = _mail_copy_split_email_addresses($email_addresses);
    foreach ($email_addresses_array as $email_address) {
      if (!valid_email_address($email_address)) {
        form_set_error($field_name, t('Please enter valid e-mail addresses. Invalid email address: @mailaddress', array('@mailaddress' => $email_address)));
      }
    }
  }
}
